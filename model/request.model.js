const mongoose = require('mongoose')

const requestSchema = new mongoose.Schema({
   senderName:{
       type:String
   },
   receiverName:{
       type:String
   }

})

mongoose.model('requests', requestSchema);