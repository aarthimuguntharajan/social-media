const mongoose = require('mongoose')

const friendSchema = new mongoose.Schema({
   user1:{
       type:String
   },
   user2:{
       type:String
   }

})

mongoose.model('friends', friendSchema);