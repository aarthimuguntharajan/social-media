//const express = require('express')
//const app = express();
const port = 8000;
const dotenv = require('dotenv')
dotenv.config()

require('./model/index')


const app = require('./src/routes')
//const connection = 

app.get('/', (req, res) => {
  res.send('Hello World!')
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}!`)
});