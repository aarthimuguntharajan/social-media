const express = require('express')
const router = express()
const cors = require('cors')
router.use(cors())

router.get('/', (req, res) => {
  res.send('Welcome to Quanrantine And Chill')
})

router.use('/user', require('./UserAuth/authController'))




module.exports = router